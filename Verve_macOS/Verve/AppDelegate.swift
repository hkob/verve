//
//  AppDelegate.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func windowController() -> WindowController? {
        let wc = NSApp.keyWindow?.windowController
        return wc is WindowController ? wc as? WindowController : nil
    }
    
    @IBAction func changeElectron(_ sender: Any) {
        windowController()?.changeType(.electron)
    }
    
    @IBAction func changeLineCurrent(_ sender: Any) {
        windowController()?.changeType(.slCurrent)
    }
    
    @IBAction func changeCreate(_ sender: Any) {
        windowController()?.changeMode(.addMode)
    }
    
    @IBAction func changeMove(_ sender: Any) {
        windowController()?.changeMode(.editMode)
    }
    
    @IBAction func changeFix(_ sender: Any) {
        windowController()?.changeMode(.fixMode)
    }
    
    @IBAction func changeDelete(_ sender: Any) {
        windowController()?.changeMode(.deleteMode)
    }
    
    @IBAction func changeEF(_ sender: Any) {
        windowController()?.changeEF(sender)
    }
    
    @IBAction func changeMF(_ sender: Any) {
        windowController()?.changeMF(sender)
    }
    
    @IBAction func changeEFL(_ sender: Any) {
        windowController()?.changeEFL(sender)
    }
    
    @IBAction func changeEQL(_ sender: Any) {
        windowController()?.changeEQL(sender)
    }
    
    @IBAction func changeEMF(_ sender: Any) {
        windowController()?.changeEMF(sender)
    }
    
    @IBAction func changeCF(_ sender: Any) {
        windowController()?.changeCF(sender)
    }
}

