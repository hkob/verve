//
//  NSViewExtension.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/15.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import Cocoa

extension NSView {
    func makePNG() -> Data {
        let rep = bitmapImageRepForCachingDisplay(in: bounds)!
        cacheDisplay(in: bounds, to: rep)
        return rep.representation(using: .png, properties: [:])!
    }
}
