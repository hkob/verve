//
//  SheetViewController.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import Cocoa
import VerveCalc

class SheetViewController: NSViewController {
    // IBOutlet (weak)
    @IBOutlet weak var xEfSlider: NSSlider!
    @IBOutlet weak var yEfSlider: NSSlider!
    @IBOutlet weak var zEfSlider: NSSlider!
    @IBOutlet weak var efEnableButton: NSButton!
    @IBOutlet weak var xMfSlider: NSSlider!
    @IBOutlet weak var yMfSlider: NSSlider!
    @IBOutlet weak var zMfSlider: NSSlider!
    @IBOutlet weak var mfEnableButton: NSButton!
    
    // object owned by another object
    private weak var viewController: ViewController?
    
    // computed property
    open var biasElectricField: Point3DwithBool { return Point3DwithBool(value: Point3D(x: xEfSlider.doubleValue, y: yEfSlider.doubleValue, z: zEfSlider.doubleValue), enable: efEnableButton.state == .on) }
    open var biasMagneticField: Point3DwithBool { return Point3DwithBool(value: Point3D(x: xMfSlider.doubleValue, y: yMfSlider.doubleValue, z: zMfSlider.doubleValue), enable: mfEnableButton.state == .on) }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(self)
    }

    // ViewControllerがメモリにロードされた後に呼び出される
    override func viewWillAppear() {
        viewController = self.representedObject as? ViewController
        let bef = viewController!.biasElectricField
        xEfSlider.doubleValue = bef.value.x
        yEfSlider.doubleValue = bef.value.y
        zEfSlider.doubleValue = bef.value.z
        efEnableButton.state = bef.enable ? .on : .off
        let bmf = viewController!.biasMagneticField
        xMfSlider.doubleValue = bmf.value.x
        yMfSlider.doubleValue = bmf.value.y
        zMfSlider.doubleValue = bmf.value.z
        mfEnableButton.state = bmf.enable ? .on : .off
    }
    
    @IBAction func changeEfSlider(_ sender: AnyObject) {
        if efEnableButton.state == .on {
            viewController!.biasElectricField = biasElectricField
            viewController?.drawLines()
        }
    }
    @IBAction func changeMfSlider(_ sender: AnyObject) {
        if mfEnableButton.state == .on {
            viewController!.biasMagneticField = biasMagneticField
            viewController?.drawLines()
        }
    }
    
    @IBAction func resetElectricField(_ sender: AnyObject) {
        xEfSlider.doubleValue = 0.0
        yEfSlider.doubleValue = 0.0
        zEfSlider.doubleValue = 0.0
        changeEfEnable(xEfSlider)
    }
    @IBAction func resetMagneticField(_ sender: AnyObject) {
        xMfSlider.doubleValue = 0.0
        yMfSlider.doubleValue = 0.0
        zMfSlider.doubleValue = 0.0
        changeMfEnable(xMfSlider)
    }
    
    @IBAction func changeEfEnable(_ sender: AnyObject) {
        let bef = efEnableButton.state == .on ? biasElectricField : Point3DwithBool()
        viewController!.biasElectricField = bef
        viewController!.drawLines()
    }
    
    @IBAction func changeMfEnable(_ sender: AnyObject) {
        let bmf = mfEnableButton.state == .on ? biasMagneticField : Point3DwithBool()
        viewController!.biasMagneticField = bmf
        viewController!.drawLines()
    }
}
