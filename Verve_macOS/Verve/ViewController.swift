//
//  ViewController.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import Cocoa
import SpriteKit
import VerveCalc

class ViewController: NSViewController, NSWindowDelegate, SKSceneDelegate {
    @IBOutlet weak var skView: SKView!
    // owned instance
    public var draw: Bool = false

    // owned instance (private)
    private var scene: Verve2DScene! // 基本的に常に存在するので!でOk
    private var verveCalculator2D: VerveCalculator2D? // 基本的に常に存在するので!でOk

    // private computed property
    fileprivate var value: Int { return windowController!.getValue }

    // object owned by another object (weak)
    public weak var document: Document?
    private weak var windowController: WindowController?
    private weak var draggedElectronNode: ElectronNode2D? {
        didSet {
            if draggedElectronNode != nil {
                draggedSLCurrentNode = nil
                editingElectronNode = draggedElectronNode
            }
        }
    }
    private weak var draggedSLCurrentNode: SLCurrentNode2D? {
        didSet {
            if draggedSLCurrentNode != nil {
                draggedElectronNode = nil
                editingSLCurrentNode = draggedSLCurrentNode
            }
        }
    }
    private weak var editingElectronNode: ElectronNode2D? {
        didSet {
            oldValue?.unsetSelected()
            if editingElectronNode != nil {
                editingSLCurrentNode = nil
                editingElectronNode?.setSelected()
                windowController?.setEditValue(editingElectronNode!.electron!.charge)
            }
        }
    }
    private weak var editingSLCurrentNode: SLCurrentNode2D? {
        didSet {
            oldValue?.unsetSelected()
            if editingSLCurrentNode != nil {
                editingElectronNode?.unsetSelected()
                editingElectronNode = nil
                editingSLCurrentNode?.setSelected()
                windowController?.setEditValue(editingSLCurrentNode!.slCurrent!.current)
            }
        }
    }

    // computed property
//    private var getImage: NSImage {
//        let dataOfView = view.dataWithPDF(inside: view.bounds)
//        return NSImage(data: dataOfView)!
//    }

    public var version: Int { return Int(Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String)! }
    public var createSaveData: VerveSaveData { return verveCalculator2D!.createSaveData(version: version, capture: view.makePNG(), buttons: windowController!.buttonData) }
    public var biasElectricField: Point3DwithBool {
        get { return verveCalculator2D!.biasElectricField }
        set { verveCalculator2D!.biasElectricField = newValue }
    }
    public var biasMagneticField: Point3DwithBool {
        get { return verveCalculator2D!.biasMagneticField }
        set { verveCalculator2D!.biasMagneticField = newValue }
    }

    // computed property(private)
    private var editType: EditType { return windowController!.editType }
    private var editMode: EditMode { return windowController!.editMode }

    private var efView: Bool { return windowController!.efView }
    private var mfView: Bool { return windowController!.mfView }
    private var eflView: Bool { return windowController!.eflView }
    private var eqlView: Bool { return windowController!.eqlView }
    private var emfView: Bool { return windowController!.emfView }
    private var cfView: Bool { return windowController!.cfView }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let s = Verve2DScene(fileNamed: "Verve2DScene") {
            scene = s
            skView.showsFPS = true
            skView.showsNodeCount = true
            skView.ignoresSiblingOrder = true

            scene.scaleMode = .fill
            scene.backgroundColor = SKColor.black

            skView.presentScene(scene)
            scene.delegate = self
            scene.nextResponder = self
            scene.size = skView.frame.size
        }

        // Do any additional setup after loading the view.
    }
    
    override open func viewWillAppear() {
        verveCalculator2D = VerveCalculator2D()
        windowController = view.window?.windowController as? WindowController
        document = windowController?.document as? Document
        
        self.view.window?.delegate = self
        verveCalculator2D!.createTemplate()
        verveCalculator2D!.setSmoothParameter()
        if document?.loadData != nil {
            loadData()
        }
        draw = true
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    // Window close 前に確認
    open func windowShouldClose(_ sender: NSWindow) -> Bool {
        let alert = NSAlert()
        alert.addButton(withTitle: "No")
        alert.addButton(withTitle: "Yes")
        alert.messageText = "Close before saving?"
        return alert.runModal() == NSApplication.ModalResponse.alertSecondButtonReturn
    }

    override open func mouseDown(with theEvent: NSEvent) {
        let location = theEvent.location(in: scene)
        let clickNode = scene.atPoint(location)
        if clickNode is ElectronNode2D { // 点電荷をタッチ
            let electronNode = clickNode as! ElectronNode2D
            switch editMode {
            case .deleteMode:
                deleteElectron(electronNode)
            case .addMode, .editMode:
                dragElectron(electronNode)
            case .fixMode:
                if let e = electronNode.electron {
                    e.enableForceAction = !e.enableForceAction
                    electronNode.setNodeRadiusAndColor(e.charge)
                }
            default:
                break
            }
//            electronNode.physicsBody?.velocity.dx = 0
//            electronNode.physicsBody?.velocity.dy = 0


//
//
//            //                    touchNode.physicsBody?.velocity.dx = 0
//            //                    touchNode.physicsBody?.velocity.dy = 0
//            //                    enableForceActWhenTouchEnded = (touchNode as! ElectronNode2D).isEnableForceAction()
//            //(touchNode as! ElectronNode2D).changeEnableForceAction(false)
        } else if clickNode is SLCurrentNode2D {
            let slCurrentNode = clickNode as! SLCurrentNode2D
            switch editMode {
            case .deleteMode:
                deleteSLCurrent(slCurrentNode)
            case .addMode, .editMode:
                dragSLCurrent(slCurrentNode)
            default:
                break
            }
        } else {
            switch (editType, editMode) {
            case (.electron, .addMode):
                addElectron(location, charge: Double(value))
            case (.slCurrent, .addMode):
                addSLCurrent(location, current: Double(value))
            default:
                break
            }
        }
    }

    public func addElectron(_ location: CGPoint, charge: Double, drawAfterPlacing: Bool = true, enableForceAction: Bool = true) {
        let electron = Electron2D(charge: charge, position: Point2D(cgpoint: location), division: abs(Int(charge)), nPlot: verveCalculator2D!.smoothNplot, enableForceAction: enableForceAction)
        scene.addElectronNode(ElectronNode2D(electron: electron))
        verveCalculator2D!.addElectron(electron)
        if drawAfterPlacing {
            draw = true
        }
    }

    public func addSLCurrent(_ location: CGPoint, current: Double, drawAfterPlacing: Bool = true) {
        let slCurrent = SLCurrent2D(current: current, position: Point2D(cgpoint: location))
        scene.addSLCurrentNode(SLCurrentNode2D(slCurrent: slCurrent))
        verveCalculator2D!.addSLCurrent(slCurrent)
        if drawAfterPlacing {
            draw = true
        }
    }

    private func deleteElectron(_ electronNode: ElectronNode2D) {
        windowController!.changeType(EditType.electron)
        let electron = electronNode.electron!
        _ = verveCalculator2D!.removeElectron(electron)
        scene.removeElectronNode(electronNode)
        draw = true
    }

    private func deleteSLCurrent(_ slCurrentNode: SLCurrentNode2D) {
        windowController!.changeType(EditType.slCurrent)
        let slCurrent = slCurrentNode.slCurrent
        _ = verveCalculator2D!.removeSLCurrent(slCurrent!)
        scene.removeSLCurrentNode(slCurrentNode)
        draw = true
    }

    private func dragElectron(_ electronNode: ElectronNode2D) {
        windowController!.changeType(EditType.electron)
        windowController!.changeMode(EditMode.editMode)
        draggedElectronNode = electronNode
    }

    private func dragSLCurrent(_ slcurrentNode: SLCurrentNode2D) {
        windowController!.changeType(EditType.slCurrent)
        windowController!.changeMode(EditMode.editMode)
        draggedSLCurrentNode = slcurrentNode
    }

    override open func mouseDragged(with theEvent: NSEvent) {
        let location = theEvent.location(in: scene)
        if let electronNode = draggedElectronNode {
            moveElectron(electronNode, location: location)
            //                        p2pHelper.moveElectron(electronNode: touchNode as! ElectronNode2D, location: location)
        }
        if let slcurrentNode = draggedSLCurrentNode {
            moveSLCurrent(slcurrentNode, location: location)
        }
    }

    private func moveElectron(_ electronNode: ElectronNode2D, location: CGPoint) {
        verveCalculator2D!.setRoughParameter()
        electronNode.position = location
        draw = true
    }

    private func moveSLCurrent(_ slCurrentNode: SLCurrentNode2D, location: CGPoint) {
        verveCalculator2D!.setRoughParameter()
        slCurrentNode.position = location
        draw = true
    }

    override open func mouseUp(with theEvent: NSEvent) {
        if draggedElectronNode != nil {
            draggedElectronNode = nil
            verveCalculator2D!.setSmoothParameter()
            draw = true
        }
        if draggedSLCurrentNode != nil {
            draggedSLCurrentNode = nil
            verveCalculator2D!.setSmoothParameter()
            draw = true
        }
    }
    
    open func didSimulatePhysics(for escene: SKScene) {
        if windowController == nil || document == nil {
            return
        }
        if emfView {
            scene.electronNodes.forEach { node in
                node.updateElectronPosition(&draw)
                if node.electron!.enableForceAction {
                    let electron = node.electron!
                    let v = node.physicsBody!.velocity
                    let f = verveCalculator2D!.force(electron, vx: Double(v.dx), vy: Double(v.dy))
                    node.physicsBody!.applyImpulse(CGVector(dx: CGFloat(f[0] * 1e-7), dy: CGFloat(f[1] * 1e-7)))
                }
            }
        } else {
            scene.electronNodes.forEach { node in
                node.updateElectronPosition(&draw)
            }
        }
        if draw {
            drawLines()
            draw = false
        }
    }
    
    open func drawLines(){
        verveCalculator2D!.updateEfields()
        let epn = scene.lineParentNode
        if eflView && verveCalculator2D!.nElectron != 0 {
            verveCalculator2D!.calcELines()
            epn?.plotELines(verveCalculator2D!.arrowStep)
        } else {
            epn?.isHidden = true
        }
        let fen = scene.fieldExhibitNode
        if efView {
            fen?.calcElectricArrowDirection(verveCalculator2D!)
        } else if mfView {
            fen?.calcMagneticArrowDirection(verveCalculator2D!)
        } else {
            fen?.isHidden = true
        }
        let eqn = scene.equipotentialNode
        if eqlView {
            eqn?.plotEqlines(ec: verveCalculator2D!)
        } else {
            eqn?.isHidden = true
        }
        let cfn = scene.coulombForceParentNode
        if cfView {
            scene.electronNodes.forEach { node in
                let electron = node.electron!
                let v = node.physicsBody!.velocity
                let f = verveCalculator2D!.force(electron, vx: Double(v.dx), vy: Double(v.dy))
                node.coulombForceNode!.setNodeAngleAndColor(f)
            }
            cfn?.isHidden = false
        } else {
            cfn?.isHidden = true
        }
    }
    
    public func unsettledAll() {
        editingElectronNode?.unsetSelected()
        editingSLCurrentNode?.unsetSelected()
        draggedElectronNode = nil
        draggedSLCurrentNode = nil
        editingElectronNode = nil
        editingSLCurrentNode = nil
    }
    
    open func changeValue(_ value: Int) {
        let dv = Double(value)
        if let node = editingElectronNode {
            node.setNodeRadiusAndColor(dv)
            scene.lineParentNode!.replaceLinesWithElectron(node.electron!)
            draw = true
        } else if let node = editingSLCurrentNode {
            node.setNodeRadiusAndColor(dv)
            draw = true
        }
    }

    private func loadData() {
        if let ld = document?.loadData {
            ld.electronPointsX.enumerated().forEach { (i, epx) in
                let (epy, epc, epa) = (ld.electronPointsY[i], ld.electronCharges[i], ld.electronEnableForceActions[i])
                addElectron(CGPoint(x: epx, y: epy), charge: epc, drawAfterPlacing: false, enableForceAction: epa)
            }
            ld.slCurrentsPointsX.enumerated().forEach { (i, spx) in
                let (spy, spv) = (ld.slCurrentsPointsY[i], ld.slCurrentValues[i])
                addSLCurrent(CGPoint(x: spx, y: spy), current: spv, drawAfterPlacing: false)
            }
            windowController!.buttonData = (ld.efView, ld.mfView, ld.eflView, ld.eqlView, ld.emfView, ld.cfView)
            verveCalculator2D!.biasElectricField = ld.biasElectricField
            verveCalculator2D!.biasMagneticField = ld.biasMagneticField
            document?.loadData = nil
            draw = true
        }
    }
    
    open func cancelAllForce() {
        scene.cancelAllForce()
    }
    
}
