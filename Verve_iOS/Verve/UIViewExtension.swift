//
//  UIViewExtension.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/15.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import UIKit

extension UIView {
    func makePNG() -> Data {
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        let context = UIGraphicsGetCurrentContext()!
        context.setShouldAntialias(false)
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image.pngData()!
    }
}
