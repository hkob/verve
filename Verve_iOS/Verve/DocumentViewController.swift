//
//  DocumentViewController.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import UIKit
import SpriteKit
import VerveCalc

class DocumentViewController: UIViewController, SKSceneDelegate {
    @IBOutlet weak var skView: SKView!
    @IBOutlet weak var typeSelector: UISegmentedControl!
    @IBOutlet weak var modeSelector: UISegmentedControl!
    @IBOutlet weak var electronCurrentValue: UIButton!
    @IBOutlet weak var electronCurrentStepper: UIStepper!
    @IBOutlet weak var efButton: UIButton!
    @IBOutlet weak var mfButton: UIButton!
    @IBOutlet weak var eflButton: UIButton!
    @IBOutlet weak var eqlButton: UIButton!
    @IBOutlet weak var emfButton: UIButton!
    @IBOutlet weak var cfButton: UIButton!    
    
    // owned instance
    public var draw: Bool = false

    // owned instance (private)
    private var scene: Verve2DScene! // 基本的に常に存在するので!でOk
    private var verveCalculator2D: VerveCalculator2D? // 基本的に常に存在するので!でOk
    private var preValue: Double = 1.0 // stepper の前の値
    private var touchingNodeWithUITouch = [UITouch:SKNode]()
    //
    //
    //     object owned by another object (weak)
    //    public weak var document: Document?
    //    private weak var windowController: WindowController?
    private weak var editingElectronNode: ElectronNode2D? {
        didSet {
            oldValue?.unsetSelected()
            if editingElectronNode != nil {
                editingSLCurrentNode = nil
                editingElectronNode?.setSelected()
                let v = editingElectronNode!.electron!.charge
                electronCurrentStepper.value = v
                preValue = v
            }
        }
    }
    private weak var editingSLCurrentNode: SLCurrentNode2D? {
        didSet {
            oldValue?.unsetSelected()
            if editingSLCurrentNode != nil {
                editingElectronNode?.unsetSelected()
                editingElectronNode = nil
                editingSLCurrentNode?.setSelected()
                let v = editingSLCurrentNode!.slCurrent!.current
                electronCurrentStepper.value = v
                preValue = v
            }
        }
    }
    //
    // computed property
    public var editType: EditType {
        return typeSelector.selectedSegmentIndex == 0 ? EditType.electron : EditType.slCurrent
    }
    public var editMode: EditMode {
        switch modeSelector.selectedSegmentIndex {
        case 0:
            return EditMode.addMode
        case 1:
            return EditMode.editMode
        case 2:
            return EditMode.fixMode
        default:
            return EditMode.deleteMode
        }
    }
    private var value: Double { return electronCurrentStepper.value }
    public var buttonData: (Bool, Bool, Bool, Bool, Bool, Bool) {
        return (efButton.isSelected, mfButton.isSelected, eflButton.isSelected, eqlButton.isSelected, emfButton.isSelected, cfButton.isSelected)
    }
    public var version: Int { return Int(Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String)! }
    public var createSaveData: VerveSaveData { return verveCalculator2D!.createSaveData(version: version, capture: view.makePNG(),  buttons: buttonData) }

    var document: Document?
    
    func setDocument(_ document: Document, completion: @escaping () -> Void) {
        self.verveCalculator2D = VerveCalculator2D()
        self.document = document
        self.document?.viewController = self
        loadViewIfNeeded()
        
        document.open(completionHandler: { (success) in
            if success {
                DispatchQueue.main.async(execute: {
                    self.verveCalculator2D!.createTemplate()
                    self.verveCalculator2D!.setSmoothParameter()
                    self.loadData()
                    self.draw = true
                })
            }
            completion()
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let s = Verve2DScene(fileNamed: "Verve2DScene") {
            scene = s
            skView.showsFPS = true
            skView.showsNodeCount = true
            skView.ignoresSiblingOrder = true
            
            scene.scaleMode = .fill
            scene.backgroundColor = SKColor.black
            
            skView.presentScene(scene)
            scene.delegate = self
//            scene.nextResponder = self
            scene.size = skView.frame.size
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "fieldViewSegue") {
            let fvvc = (segue.destination as? FieldValuesViewController)!
            // ViewControllerのtextVC2にメッセージを設定
            fvvc.documentViewController = self
            fvvc.verveCalculator2D = verveCalculator2D
        }
    }
    
    
    @IBAction func showBackToHomeAlert() {
        let alert = UIAlertController(title: NSLocalizedString("Save changes?", comment: ""), message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {
            (action: UIAlertAction!) -> Void in
            alert.removeFromParent()
        })
        let destructiveAction = UIAlertAction(title: NSLocalizedString("Discard", comment: ""), style: .destructive, handler:{
            (action: UIAlertAction!) -> Void in
            alert.removeFromParent()
            self.dismissDocumentViewController()
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("Save", comment: ""), style: .default, handler:{
            (action:UIAlertAction!) -> Void in
            alert.removeFromParent()
            self.document!.save(to: self.document!.fileURL, for: .forOverwriting, completionHandler: { (success) -> Void in
                if success {
                    print("File overwrite Ok")
                } else {
                    print("File overwrite failed")
                }
            })
            self.dismissDocumentViewController()
        })
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        alert.addAction(destructiveAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func dismissDocumentViewController() {
        dismiss(animated: true) {
            self.document?.close(completionHandler: nil)
        }
    }

    public func displayValue() {
        electronCurrentValue.setTitle("\(Int(value))[\(editType == EditType.electron ? "C" : "A")]", for: UIControl.State.normal)
    }
    
    @IBAction func changeEF(_ sender: Any) {
        efButton.isSelected = !efButton.isSelected
        if efButton.isSelected {
            mfButton.isSelected = false
        }
        draw = true
    }
    
    @IBAction func changeMF(_ sender: Any) {
        mfButton.isSelected = !mfButton.isSelected
        if mfButton.isSelected {
            efButton.isSelected = false
        }
        draw = true
    }
    
    @IBAction func changeEFL(_ sender: Any) {
        eflButton.isSelected = !eflButton.isSelected
        draw = true
    }
    
    @IBAction func changeEQL(_ sender: Any) {
        eqlButton.isSelected = !eqlButton.isSelected
        draw = true
    }
    
    @IBAction func changeEMF(_ sender: Any) {
        emfButton.isSelected = !emfButton.isSelected
        if !emfButton.isSelected {
            cancelAllForce()
        }
        draw = true
    }
    
    @IBAction func changeCF(_ sender: Any) {
        cfButton.isSelected = !cfButton.isSelected
        draw = true
    }
    
    public func changeType(_ type: EditType) {
        typeSelector.selectedSegmentIndex = type.rawValue
        displayValue()
    }
    
    @IBAction func changeTypeByButton(_ sender: Any) {
        changeMode(EditMode.addMode)
    }
    
    private func changeValueEnabled(_ flag: Bool) {
        electronCurrentValue.isHidden = !flag
        electronCurrentStepper.isHidden = !flag
        electronCurrentStepper.isEnabled = flag
    }

    @IBAction func changeModeByButton(_ sender: Any) {
        switch modeSelector.selectedSegmentIndex {
        case EditMode.addMode.rawValue:
            unsettledAll()
            changeValueEnabled(true)
        case EditMode.editMode.rawValue:
            changeValueEnabled(true)
            break
        case EditMode.fixMode.rawValue:
            unsettledAll()
            changeValueEnabled(false)
        default:
            unsettledAll()
            changeValueEnabled(false)
            break
        }
        displayValue()
    }
    
    public func changeMode(_ mode: EditMode) {
        modeSelector.selectedSegmentIndex = mode.rawValue
        changeModeByButton(modeSelector)
    }
    
    @IBAction func changeStepperValue(_ sender: UIStepper) {
        let nowValue = sender.value
        if nowValue == 0 {
            sender.value = preValue > 0 ? -1.0 : 1.0
        }
        changeValue(sender.value)
        displayValue()
    }

//    override var representedObject: Any? {
//        didSet {
//            // Update the view, if already loaded.
//        }
//    }
//
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: scene)
            let touchNode = scene.atPoint(location)
            touchingNodeWithUITouch[touch] = touchNode
            if touchNode is ElectronNode2D { // 点電荷をタッチ
                let electronNode = touchNode as! ElectronNode2D
                switch editMode {
                case .deleteMode:
                    deleteElectron(electronNode)
                case .addMode, .editMode:
                    dragElectron(electronNode)
                case .fixMode:
                    if let e = electronNode.electron {
                        e.enableForceAction = !e.enableForceAction
                        electronNode.setNodeRadiusAndColor(e.charge)
                    }
                default:
                    break
                }
                // electronNode.physicsBody?.velocity.dx = 0
                // electronNode.physicsBody?.velocity.dy = 0
            } else if touchNode is SLCurrentNode2D {
                let slCurrentNode = touchNode as! SLCurrentNode2D
                switch editMode {
                case .deleteMode:
                    deleteSLCurrent(slCurrentNode)
                case .addMode, .editMode:
                    dragSLCurrent(slCurrentNode)
                default:
                    break
                }
            } else {
                switch (editType, editMode) {
                case (.electron, .addMode):
                    addElectron(location, charge: value)
                case (.slCurrent, .addMode):
                    addSLCurrent(location, current: value)
                default:
                    break
                }
            }
        }
    }
        
    public func addElectron(_ location: CGPoint, charge: Double, drawAfterPlacing: Bool = true, enableForceAction: Bool = true) {
        let electron = Electron2D(charge: charge, position: Point2D(cgpoint: location), division: abs(Int(charge)), nPlot: verveCalculator2D!.smoothNplot, enableForceAction: enableForceAction)
        scene.addElectronNode(ElectronNode2D(electron: electron))
        verveCalculator2D!.addElectron(electron)
        if drawAfterPlacing {
            draw = true
        }
    }

    public func addSLCurrent(_ location: CGPoint, current: Double, drawAfterPlacing: Bool = true) {
        let slCurrent = SLCurrent2D(current: current, position: Point2D(cgpoint: location))
        scene.addSLCurrentNode(SLCurrentNode2D(slCurrent: slCurrent))
        verveCalculator2D!.addSLCurrent(slCurrent)
        if drawAfterPlacing {
            draw = true
        }
    }

    private func deleteElectron(_ electronNode: ElectronNode2D) {
        changeType(EditType.electron)
        let electron = electronNode.electron!
        _ = verveCalculator2D!.removeElectron(electron)
        scene.removeElectronNode(electronNode)
        draw = true
    }
    
    private func deleteSLCurrent(_ slCurrentNode: SLCurrentNode2D) {
        changeType(EditType.slCurrent)
        let slCurrent = slCurrentNode.slCurrent
        _ = verveCalculator2D!.removeSLCurrent(slCurrent!)
        scene.removeSLCurrentNode(slCurrentNode)
        draw = true
    }
    
    private func dragElectron(_ electronNode: ElectronNode2D) {
        changeType(EditType.electron)
        changeMode(EditMode.editMode)
        editingElectronNode = electronNode
        electronCurrentStepper.value = electronNode.electron!.charge
        displayValue()
    }
    
    private func dragSLCurrent(_ slcurrentNode: SLCurrentNode2D) {
        changeType(EditType.slCurrent)
        changeMode(EditMode.editMode)
        editingSLCurrentNode = slcurrentNode
        electronCurrentStepper.value = slcurrentNode.slCurrent!.current
        displayValue()
    }
    
    override open func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            if let touchNode = touchingNodeWithUITouch[touch]{
                let tl = touch.location(in: scene)
                let tpl = touch.previousLocation(in: scene)
                let tnl = touchNode.position
                let location = CGPoint(x: tl.x - tpl.x + tnl.x, y: tl.y - tpl.y + tnl.y)
                if touchNode is ElectronNode2D {
                    let electronNode = touchNode as! ElectronNode2D
                    moveElectron(electronNode, location: location)
                    // p2pHelper.moveElectron(electronNode: touchNode as! ElectronNode2D, location: location)
                } else if touchNode is SLCurrentNode2D {
                    let slcurrentNode = touchNode as! SLCurrentNode2D
                    moveSLCurrent(slcurrentNode, location: location)
                }
            }
        }
    }
    
    private func moveElectron(_ electronNode: ElectronNode2D, location: CGPoint) {
        verveCalculator2D!.setRoughParameter()
        electronNode.position = location
        draw = true
    }
    
    private func moveSLCurrent(_ slCurrentNode: SLCurrentNode2D, location: CGPoint) {
        verveCalculator2D!.setRoughParameter()
        slCurrentNode.position = location
        draw = true
    }
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            if let touchNode = touchingNodeWithUITouch.removeValue(forKey: touch) {
                if touchNode is ElectronNode2D {
                    let electronNode = touchNode as! ElectronNode2D
                    editingElectronNode = electronNode
                } else if touchNode is SLCurrentNode2D {
                    let slcurrentNode = touchNode as! SLCurrentNode2D
                    editingSLCurrentNode = slcurrentNode
                }
            }
        }
        verveCalculator2D!.setSmoothParameter()
        draw = true
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesEnded(touches, with: event)
    }
    
    open func didSimulatePhysics(for escene: SKScene) {
        if document == nil {
            return
        }
        if emfButton.isSelected {
            scene.electronNodes.forEach { node in
                node.updateElectronPosition(&draw)
                if node.electron!.enableForceAction {
                    let electron = node.electron!
                    let v = node.physicsBody!.velocity
                    let f = verveCalculator2D!.force(electron, vx: Double(v.dx), vy: Double(v.dy))
                    node.physicsBody!.applyImpulse(CGVector(dx: CGFloat(f[0] * 1e-7), dy: CGFloat(f[1] * 1e-7)))
                }
            }
        } else {
            scene.electronNodes.forEach { node in
                node.updateElectronPosition(&draw)
            }
        }
        if draw {
            drawLines()
            draw = false
        }
    }
    
    open func drawLines(){
        verveCalculator2D!.updateEfields()
        let epn = scene.lineParentNode
        if eflButton.isSelected && verveCalculator2D!.nElectron != 0 {
            verveCalculator2D!.calcELines()
            epn?.plotELines(verveCalculator2D!.arrowStep)
        } else {
            epn?.isHidden = true
        }
        let fen = scene.fieldExhibitNode
        if efButton.isSelected {
            fen?.calcElectricArrowDirection(verveCalculator2D!)
        } else if mfButton.isSelected {
            fen?.calcMagneticArrowDirection(verveCalculator2D!)
        } else {
            fen?.isHidden = true
        }
        let eqn = scene.equipotentialNode
        if eqlButton.isSelected {
            eqn?.plotEqlines(ec: verveCalculator2D!)
        } else {
            eqn?.isHidden = true
        }
        let cfn = scene.coulombForceParentNode
        if cfButton.isSelected {
            scene.electronNodes.forEach { node in
                let electron = node.electron!
                let v = node.physicsBody!.velocity
                let f = verveCalculator2D!.force(electron, vx: Double(v.dx), vy: Double(v.dy))
                node.coulombForceNode!.setNodeAngleAndColor(f)
            }
            cfn?.isHidden = false
        } else {
            cfn?.isHidden = true
        }
    }

    public func unsettledAll() {
        editingElectronNode?.unsetSelected()
        editingSLCurrentNode?.unsetSelected()
        editingElectronNode = nil
        editingSLCurrentNode = nil
    }

    open func changeValue(_ value: Double) {
        preValue = value
        if let node = editingElectronNode {
            node.setNodeRadiusAndColor(value)
            scene.lineParentNode!.replaceLinesWithElectron(node.electron!)
            draw = true
        } else if let node = editingSLCurrentNode {
            node.setNodeRadiusAndColor(value)
            draw = true
        }
    }

    private func loadData() {
        if let ld = document?.loadData {
            ld.electronPointsX.enumerated().forEach { (i, epx) in
                let (epy, epc, epa) = (ld.electronPointsY[i], ld.electronCharges[i], ld.electronEnableForceActions[i])
                addElectron(CGPoint(x: epx, y: epy), charge: epc, drawAfterPlacing: false, enableForceAction: epa)
            }
            ld.slCurrentsPointsX.enumerated().forEach { (i, spx) in
                let (spy, spv) = (ld.slCurrentsPointsY[i], ld.slCurrentValues[i])
                addSLCurrent(CGPoint(x: spx, y: spy), current: spv, drawAfterPlacing: false)
            }
            efButton.isSelected = ld.efView
            mfButton.isSelected = ld.mfView
            eflButton.isSelected = ld.eflView
            eqlButton.isSelected = ld.eqlView
            emfButton.isSelected = ld.emfView
            cfButton.isSelected = ld.cfView
            verveCalculator2D!.biasElectricField = ld.biasElectricField
            verveCalculator2D!.biasMagneticField = ld.biasMagneticField
            document?.loadData = nil
            draw = true
        }
    }

    open func cancelAllForce() {
        scene.cancelAllForce()
    }
}
