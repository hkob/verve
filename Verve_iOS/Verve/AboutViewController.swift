//
//  AboutViewController.swift
//  Verve
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/14.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//  Copyright © 2018年 Hiroyuki KOBAYASHI. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        versionLabel.text = "\(version) (\(build))"
        descriptionTextView.text = NSLocalizedString("description", comment: "")
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
