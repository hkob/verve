//
//  EquipotentialNode2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import SpriteKit

public class EquipotentialNode2D: SKNode {
    // owned instance (private)
    private var width: Int = 0
    private var height: Int = 0
    private var size: Int = 0
    private var exponent: [Double] = []
    private var filtered_exponent: [Double] = []
    private var buffer: [UInt8] = []
    private var node : SKSpriteNode?
    private var data : Data?
    
    private static let filter: [Double] = [0.0, 1.0, 0.0, 1.0, -4.0, 1.0, 0.0, 1.0, 0.0];

    public convenience init(screenSize: CGSize){
        self.init()
        width = Int(screenSize.width)
        height = Int(screenSize.height)
        size = width * height
        exponent = [Double](repeating: 0.0, count: size)
        filtered_exponent = [Double](repeating: 0.0, count: size)
        buffer = [UInt8](repeating: 0, count: size * 4)
        data = Data(bytesNoCopy: &buffer, count: size * 4, deallocator: .none)
        let texture = SKTexture(data: data!, size: CGSize(width: width, height: height), flipped: false)
        node = SKSpriteNode(texture: texture)
        node!.position = CGPoint(x: width / 2, y: height / 2)
        self.addChild(node!)
    }
    
    open func plotEqlines(ec: VerveCalculator2D) {
        let ea = ec.eafields
        let sign = ec.tmp2fields
        for i in 0 ..< width * height {
            exponent[i] = (Double(ea[i].exponentBitPattern) * Double(sign[i]))
        }
        nsfil(exponent, nr: height, nc: width, fil: EquipotentialNode2D.filter, out: &filtered_exponent)
        filtered_exponent.enumerated().forEach { (i, v) in
            let x = UInt8(v > 0 ? 255 : 0)
            let j = i * 4
            buffer[j] = x
            buffer[j+1] = x
            buffer[j+2] = x
            buffer[j+3] = x
        }
        let texture = SKTexture(data: data!, size: CGSize(width: width, height: height), flipped: false)
        node?.texture = texture

        isHidden = false
    }

}
