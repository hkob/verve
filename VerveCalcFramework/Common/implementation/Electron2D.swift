//
//  Electron2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public class Electron2D: NSObject, Sequence {
    // owned instance
    open var position: Point2D
    open var enableForceAction: Bool
    open var rotateStartAngle: Double

    // owned instance (didSet)
    open var charge: Double = 0.0 {
        didSet {
            chargeSign = charge >= 0.0 ? 1.0 : -1.0
            nLine = Int(charge * chargeSign)
            let c = 2.0 * Double.pi / Double(nLine)
            for i in 0 ..< self.nLine {
                lines[i].theta = c * Double(i)
            }
        }
    }
//
    // owned instance (private set)
    open fileprivate(set) var lines: [Line2D] = []
    open private(set) var chargeSign: Double
    open private(set) var nLine: Int

    // computed property
    open var x: Double { return position.x }
    open var y: Double { return position.y }

//    // constant
//    fileprivate let e0: Double = 8.85418782e-12
//
    public init(charge: Double, position: Point2D, division: Int, nPlot: Int, enableForceAction efa: Bool = true) {
        self.position = position
        self.charge = charge
        nLine = division > 0 ? division : 1
        enableForceAction = efa
        let c = 2.0 * Double.pi / Double(nLine)
        for i in 0 ..< 10 {
            lines.append(Line2D(nPlot: nPlot, theta: c * Double(i), position: position, index: i))
        }
        chargeSign = charge >= 0.0 ? 1.0 : -1.0
        rotateStartAngle = Double.nan
    }

    public func appendInputs(theta: Double, recalc: inout Bool) {
        var fixed = false
        for l in lines {
            if l.fixedTheta.isNaN && !l.reachElectron{
                l.fixedTheta = theta
                fixed = true
                if l.used {
                    recalc = true
                }
                break
            }
        }
        if !fixed {
            recalc = true
        }
        if rotateStartAngle.isNaN {
            rotateStartAngle = theta + 2.0 * Double.pi / Double(lines.count) - Double.pi
            recalc = true
        }
    }

    public func clearUsed() {
        self.forEach { (l) in
            l.used = false
        }
    }

    public func clearRotateAngle() {
        rotateStartAngle = Double.nan
        self.forEach { (l) in
            l.fixedTheta = Double.nan
            l.used = false
            l.reachElectron = false
        }
    }

//    // ベクトル化のため未使用
//    //    public func addExy(position: Point2D, inout e: Point2D) {
//    //        let d = position.diff(position)
//    //        if d.r != 0.0 {
//    //            let r = d.r
//    //            let c = _charge / (4.0 * M_PI * e0 * r * r * r)
//    //            e.addOther(d, multiply: c)
//    //        }
//    //    }
//
    public func makeIterator() -> AnyIterator<Line2D> {
        var index : Int = 0
        return AnyIterator {
            if index < self.nLine {
                let ans = self.lines[index]
                index += 1
                return ans
            } else {
                return .none
            }
        }
    }

//    public func normalizeTheta(_ theta: Double) -> Double {
//        return theta > Double.pi ? theta - Double.pi : theta < -Double.pi ? theta + Double.pi : theta
//    }

}
