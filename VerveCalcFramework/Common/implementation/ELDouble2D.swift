//
//  ELDouble2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public class ELDouble2D: NSObject {
    // owned instance
    public var point: Point2D
    public var index: Int
    
    // computed property
    public var x: Double { return point.x }
    public var y: Double { return point.y }
    public var r: Double { return point.r }
    override public var description : String { return "(\( index ): \( point.description ))" }
    public var angle: Double { return point.angle }
    
    /**
     - parameter x:default is 0.0
     - parameter y:default is 0.0
     - parameter index:default is 0.0
     */
    public init(x: Double = 0.0, y: Double = 0.0, index: Int = 0) {
        point = Point2D(x: x, y: y)
        self.index = index < 0 ? 0 : index
    }
    
    /**
     座標を変更
     - parameter x:default is 0.0
     - parameter y:default is 0.0
     */
    public func setValueX(_ x: Double = 0.0, y: Double = 0.0) {
        point.setValueX(x, y: y)
    }

    /**
     座標を変更
     - parameter x:default is 0.0
     - parameter y:default is 0.0
     - parameter index
     */
    public func setValueX(_ x: Double = 0.0, y: Double = 0.0, index: Int) {
        point.setValueX(x, y: y)
        self.index = index
    }

    /**
     座標を変更 (_x += dx, _y += dy)
     - parameter dx:default is 0.0
     - parameter dy:default is 0.0
     */
    public func addValueX(_ x: Double = 0.0, y: Double = 0.0) {
        point.addValueX(x, y: y)
    }

    /**
     座標を変更 (_x += c * o.x, _y += c * o.y)
     - parameter c: Double
     - parameter o: ELDouble2D
     */
    public func addOther(_ other: ELDouble2D, multiply: Double = 1.0) {
        point.addOther(other.point, multiply: multiply)
    }

    /**
     差を作成 (_x - o.x, _y - o.y)
     - parameter o:
     */
    public func diff(_ other: ELDouble2D) -> ELDouble2D {
        return ELDouble2D(x: self.x - other.x, y: self.y - other.y)
    }

    public func isEqual(_ object: AnyObject?) -> Bool {
        if let object = object as? ELDouble2D {
            return self.x == object.x && self.y == object.y
        } else {
            return false
        }
    }

}
