//
//  FieldExhibitNode2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import SpriteKit

public class FieldExhibitNode2D: SKNode {
    // owned instance (private)
    //    private var maxOfEabs:Double = 0
    private var renderingWithColor = true
    private var renderingWithLength = true
    private var pxs: [Double] = []
    private var pys: [Double] = []

    public convenience init(screenSize: CGSize, arrowDistance: CGFloat){
        self.init()
        let width = screenSize.width
        let height = screenSize.height

        func addArrowNode(_ point: CGPoint){
            let arrowNode = SKShapeNode()
            arrowNode.zPosition = -1
            arrowNode.position = point
            arrowNode.lineWidth = 3
            addChild(arrowNode)
        }

        let hwidth = width / 2.0 - 1
        let hheight = height / 2.0 - 1
        (0 ... Int(hheight / arrowDistance)).forEach { i in
            let y = CGFloat(i) * arrowDistance
            (0 ... Int(hwidth / arrowDistance)).forEach { j in
                let x = CGFloat(j) * arrowDistance
                addArrowNode(CGPoint(x: hwidth + x, y: hheight + y))
                if j != 0 {
                    addArrowNode(CGPoint(x: hwidth - x, y: hheight + y))
                }
            }
            if i != 0 {
                (0 ... Int(hwidth / arrowDistance)).forEach { j in
                    let x = CGFloat(j) * arrowDistance
                    addArrowNode(CGPoint(x: hwidth + x, y: hheight - y))
                    if j != 0 {
                        addArrowNode(CGPoint(x: hwidth - x, y: hheight - y))
                    }
                }
            }
        }
        let ps = children.map { $0.position }
        pxs = ps.map { Double($0.x) }
        pys = ps.map { Double($0.y) }
    }

    fileprivate func myLog(_ exy: Double) -> Double {
        let absExy = fabs(exy) + 1.0
        return (absExy > 1E4) ? (log10(absExy) - 4.0) * 6.0 : 0.0
    }
    
    func myLength(_ hxy:Double) -> Double {
        return fabs(hxy)*800 > 20.0 ? 20.0 : fabs(hxy)*800
    }

    private func arrowPathEx(_ ex: Double, ey: Double, vlen: Double) -> BezierPath {
        let path = BezierPath()
        let angle = atan2(ey, ex)
        let length = renderingWithLength ? vlen : 20.0
        let lengthX = length * cos(angle)
        let lengthY = length * sin(angle)
        let lengthForRect = length * 0.4
        let lengthXPlusDeg = lengthForRect * cos(angle + 0.9)
        let lengthYPlusDeg = lengthForRect * sin(angle + 0.9)
        let lengthXMinusDeg = lengthForRect * cos(angle - 0.9)
        let lengthYMinusDeg = lengthForRect * sin(angle - 0.9)
        path.removeAllPoints()
        path.move(to: CGPoint(x: -lengthX, y: -lengthY))
        path.line(to: CGPoint(x: lengthX, y: lengthY))
        path.move(to: CGPoint(x: lengthXPlusDeg, y: lengthYPlusDeg))
        path.line(to: CGPoint(x: lengthX*0.7, y: lengthY*0.7))
        path.line(to: CGPoint(x: lengthXMinusDeg, y: lengthYMinusDeg))
        path.line(to: CGPoint(x: lengthX, y: lengthY))
        path.close()
        return path
    }
    
    open func calcElectricArrowDirection(_ verveCalculator: VerveCalculator2D){
        let (exs, eys, ers) = verveCalculator.calcExyByPxs(pxs, pys: pys)
        children.enumerated().forEach { (index, arrowNode) in
            let shapeNodeArrowNode = arrowNode as! SKShapeNode
            let eabs = ers[index]
            let path = arrowPathEx(exs[index], ey: eys[index], vlen: myLog(eabs))
            shapeNodeArrowNode.path = path.cgPath
            if renderingWithColor {
                let len = myLog(eabs)
                let hue = (len > 20.0 ? 0.0 : 20.0 - len) / 20.0 * 0.8
                shapeNodeArrowNode.strokeColor = SKColor(hue: CGFloat(hue), saturation: 1, brightness: 1, alpha: 1)
            } else {
                shapeNodeArrowNode.strokeColor = SKColor.white
            }
        }
        isHidden = false
    }
    
    open func calcMagneticArrowDirection(_ verveCalculator: VerveCalculator2D){
        let (hxs, hys, hrs) = verveCalculator.calcHxyByPxs(pxs, pys: pys)
        children.enumerated().forEach { (index, arrowNode) in
            let shapeNodeArrowNode = arrowNode as! SKShapeNode
            let habs = hrs[index]
            let path = arrowPathEx(hxs[index], ey: hys[index], vlen: myLength(habs))
            shapeNodeArrowNode.path = path.cgPath
            if renderingWithColor {
                let len = myLength(habs)
                let hue = (len > 20.0 ? 0.0 : 20.0 - len) / 20.0 * 0.8
                shapeNodeArrowNode.strokeColor = SKColor(hue: CGFloat(hue), saturation: 1, brightness: 1, alpha: 1)
            } else {
                shapeNodeArrowNode.strokeColor = SKColor.white
            }
        }
        isHidden = false
    }
    //    //
    //    //    public override init(){
    //    //        super.init()
    //    //    }
    //    //
    //    //    required public init?(coder aDecoder: NSCoder) {
    //    //        fatalError("init(coder:) has not been implemented")
    //    //    }
    //    //
    //

}
