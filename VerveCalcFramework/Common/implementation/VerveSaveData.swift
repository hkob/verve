//
//  VerveSaveData.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/07.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import Foundation

public enum SaveKeys: String {
    case Version = "Version"
    case Capture = "Capture"
    case Date = "Date"
    case Title = "Title"
    case ElectronPointsX = "ElectronPointsX"
    case ElectronPointsY = "ElectronPointsY"
    case ElectronCharges = "ElectronCharges"
    case ElectronEnableForceActions = "ElectronEnableForceActions"
    case BiasElectricFieldX = "BiasElectricFieldX"
    case BiasElectricFieldY = "BiasElectricFieldY"
    case BiasElectricFieldZ = "BiasElectricFieldZ"
    case BiasElectricFieldEnable = "BiasElectricFieldEnable"
    case SLCurrentsPointsX = "SLCurrentsPointsX"
    case SLCurrentsPointsY = "SLCurrentsPointsY"
    case SLCurrentValues = "SLCurrentValues"
    case BiasMagneticFieldX = "BiasMagneticFieldX"
    case BiasMagneticFieldY = "BiasMagneticFieldY"
    case BiasMagneticFieldZ = "BiasMagneticFieldZ"
    case BiasMagneticFieldEnable = "BiasMagneticFieldEnable"
    case EfView = "EfView"
    case MfView = "MfView"
    case EflView = "EflView"
    case EqlView = "EqlView"
    case EmfView = "EmfView"
    case CfView = "CfView"
}

extension NSCoder {
    func decodeData(_ key: SaveKeys) -> Data {
        return decodeObject(forKey: key.rawValue) as? Data ?? Data()
    }

    func decodeInt(_ key: SaveKeys) -> Int {
        return decodeObject(forKey: key.rawValue) as? Int ?? 0
    }
    
    func decodeString(_ key: SaveKeys) -> String {
        return decodeObject(forKey: key.rawValue) as? String ?? "Unknown"
    }

    func decodeDoubleArray(_ key: SaveKeys) -> [Double] {
        return decodeObject(forKey: key.rawValue) as? [Double] ?? [Double]()
    }

    func decodeBoolArray(_ key: SaveKeys) -> [Bool] {
        return decodeObject(forKey: key.rawValue) as? [Bool] ?? [Bool]()
    }

    func decodePoint3DwithBool(keyx: SaveKeys, keyy: SaveKeys, keyz: SaveKeys, keye: SaveKeys) -> Point3DwithBool {
        let x = decodeObject(forKey: keyx.rawValue) as? Double ?? 0.0
        let y = decodeObject(forKey: keyy.rawValue) as? Double ?? 0.0
        let z = decodeObject(forKey: keyz.rawValue) as? Double ?? 0.0
        let e = decodeObject(forKey: keye.rawValue) as? Bool ?? true
        return Point3DwithBool(value: Point3D(x: x, y: y, z: z), enable: e)
    }

    func decodeBool(_ key: SaveKeys) -> Bool {
        return decodeObject(forKey: key.rawValue) as? Bool ?? (key == SaveKeys.EfView)
    }

    func encodePoint3DwithBool(_ p3b: Point3DwithBool, keyx: SaveKeys, keyy: SaveKeys, keyz: SaveKeys, keye: SaveKeys) {
        encodeValue(p3b.value.x as AnyObject, key: keyx)
        encodeValue(p3b.value.y as AnyObject, key: keyy)
        encodeValue(p3b.value.z as AnyObject, key: keyz)
        encodeValue(p3b.enable as AnyObject, key: keye)
    }

    func encodeValue(_ objv: AnyObject, key: SaveKeys) {
        encode(objv, forKey: key.rawValue)
    }
}

public class VerveSaveData: NSObject, NSCoding {
    // owned objects public get private set
    open private(set) var electronPointsX = [Double]()
    open private(set) var electronPointsY = [Double]()
    open private(set) var electronCharges = [Double]()
    open private(set) var electronEnableForceActions = [Bool]()
    open private(set) var slCurrentsPointsX = [Double]()
    open private(set) var slCurrentsPointsY = [Double]()
    open private(set) var slCurrentValues = [Double]()
    open private(set) var efView = true
    open private(set) var mfView = false
    open private(set) var eflView = false
    open private(set) var eqlView = false
    open private(set) var emfView = false
    open private(set) var cfView = false
    open private(set) var biasElectricField: Point3DwithBool
    open private(set) var biasMagneticField: Point3DwithBool

    // owned objects
    private let version: Int
    private let capture: Data
    private let date: String
    private let title: String

    public init(version v: Int, capture c: Data, date d: Date, title t: String, buttons: (Bool, Bool, Bool, Bool, Bool, Bool), verveCalculator2D vc: VerveCalculator2D) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        version = v
        capture = c
        date = formatter.string(from: d)
        title = t

        electronPointsX = vc.electrons.map { $0.position.x }
        electronPointsY = vc.electrons.map { $0.position.y }
        electronCharges = vc.electrons.map { $0.charge }
        electronEnableForceActions = vc.electrons.map { $0.enableForceAction }

        slCurrentsPointsX = vc.slCurrents.map { $0.position.x }
        slCurrentsPointsY = vc.slCurrents.map { $0.position.y }
        slCurrentValues = vc.slCurrents.map { $0.current }

        biasElectricField = vc.biasElectricField
        biasMagneticField = vc.biasMagneticField
        (efView, mfView, eflView, eqlView, emfView, cfView) = buttons
    }

    public required init?(coder aDecoder: NSCoder) {
        version = aDecoder.decodeInt(.Version)
        capture = aDecoder.decodeData(.Capture)
        date = aDecoder.decodeString(.Date)
        title = aDecoder.decodeString(.Title)
        electronPointsX = aDecoder.decodeDoubleArray(.ElectronPointsX)
        electronPointsY = aDecoder.decodeDoubleArray(.ElectronPointsY)
        electronCharges = aDecoder.decodeDoubleArray(.ElectronCharges)
        electronEnableForceActions = aDecoder.decodeBoolArray(.ElectronEnableForceActions)
        biasElectricField = aDecoder.decodePoint3DwithBool(keyx: .BiasElectricFieldX, keyy: .BiasElectricFieldY, keyz: .BiasElectricFieldZ, keye: .BiasElectricFieldEnable)
        slCurrentsPointsX = aDecoder.decodeDoubleArray(.SLCurrentsPointsX)
        slCurrentsPointsY = aDecoder.decodeDoubleArray(.SLCurrentsPointsY)
        slCurrentValues = aDecoder.decodeDoubleArray(.SLCurrentValues)
        biasMagneticField = aDecoder.decodePoint3DwithBool(keyx: .BiasMagneticFieldX, keyy: .BiasMagneticFieldY, keyz: .BiasMagneticFieldZ, keye: .BiasMagneticFieldEnable)
        efView = aDecoder.decodeBool(.EfView)
        mfView = aDecoder.decodeBool(.MfView)
        eflView = aDecoder.decodeBool(.EflView)
        eqlView = aDecoder.decodeBool(.EqlView)
        emfView = aDecoder.decodeBool(.EmfView)
        cfView = aDecoder.decodeBool(.CfView)
    }

    open func encode(with aCoder: NSCoder) {
        aCoder.encodeValue(version as AnyObject, key: .Version)
        aCoder.encodeValue(capture as AnyObject, key: .Capture)
        aCoder.encodeValue(date as AnyObject, key: .Date)
        aCoder.encodeValue(title as AnyObject, key: .Title)
        aCoder.encodeValue(electronPointsX as AnyObject, key: .ElectronPointsX)
        aCoder.encodeValue(electronPointsY as AnyObject, key: .ElectronPointsY)
        aCoder.encodeValue(electronCharges as AnyObject, key: .ElectronCharges)
        aCoder.encodeValue(electronEnableForceActions as AnyObject, key: .ElectronEnableForceActions)
        aCoder.encodePoint3DwithBool(biasElectricField, keyx: .BiasElectricFieldX, keyy: .BiasElectricFieldY, keyz: .BiasElectricFieldZ, keye: .BiasElectricFieldEnable)
        aCoder.encodeValue(slCurrentsPointsX as AnyObject, key: .SLCurrentsPointsX)
        aCoder.encodeValue(slCurrentsPointsY as AnyObject, key: .SLCurrentsPointsY)
        aCoder.encodeValue(slCurrentValues as AnyObject, key: .SLCurrentValues)
        aCoder.encodePoint3DwithBool(biasMagneticField, keyx: .BiasMagneticFieldX, keyy: .BiasMagneticFieldY, keyz: .BiasMagneticFieldZ, keye: .BiasMagneticFieldEnable)
        aCoder.encodeValue(efView as AnyObject, key: .EfView)
        aCoder.encodeValue(mfView as AnyObject, key: .MfView)
        aCoder.encodeValue(eflView as AnyObject, key: .EflView)
        aCoder.encodeValue(eqlView as AnyObject, key: .EqlView)
        aCoder.encodeValue(emfView as AnyObject, key: .EmfView)
        aCoder.encodeValue(cfView as AnyObject, key: .CfView)
    }
}
