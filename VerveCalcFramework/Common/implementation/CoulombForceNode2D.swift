//
//  CoulombForceNode2D.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import SpriteKit

public class CoulombForceNode2D: SKShapeNode {
    // owned instance
    private var renderingWithLength = true

    // owned instance (private set)
    open private(set) var electron: Electron2D?

    public override init() {
        super.init()
    }

    public convenience init(electron: Electron2D){
        self.init()
        self.electron = electron
        setNodeAngleAndColor([0, 0])
        self.position = CGPoint(x: CGFloat(electron.x), y: CGFloat(electron.y))
        self.name = "CoulombForceNode2D"
        self.zPosition = 2
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func myLog(_ exy: Double) -> Double {
        let absExy = fabs(exy) + 1.0
        return (absExy > 1E4) ? (log10(absExy) - 4.0) * 12.0 : 0.0
    }

    fileprivate func arrowPathFx(_ fx: Double, fy: Double, vlen: Double) -> BezierPath {
        let path = BezierPath()
        let angle = atan2(fy, fx)
        let length = renderingWithLength ? vlen : 20.0
        let lengthX = length * cos(angle)
        let lengthY = length * sin(angle)
        let lengthForRect = length * 0.8
        let lengthXPlusDeg = lengthForRect * cos(angle + 0.3)
        let lengthYPlusDeg = lengthForRect * sin(angle + 0.3)
        let lengthXMinusDeg = lengthForRect * cos(angle - 0.3)
        let lengthYMinusDeg = lengthForRect * sin(angle - 0.3)
        path.removeAllPoints()
        path.move(to: CGPoint(x: 0, y: 0))
        path.line(to: CGPoint(x: lengthX, y: lengthY))
        path.move(to: CGPoint(x: lengthXPlusDeg, y: lengthYPlusDeg))
        path.line(to: CGPoint(x: lengthX*0.9, y: lengthY*0.9))
        path.line(to: CGPoint(x: lengthXMinusDeg, y: lengthYMinusDeg))
        path.line(to: CGPoint(x: lengthX, y: lengthY))
        path.close()
        return path
    }

    open func setNodeAngleAndColor(_ force: [Double]){
        //        let absChargeValue = fabs(CGFloat(chargeValue))
        //        let radius = (2.0*absChargeValue+10.0)*1.5
        //        let color = chargeValue < 0 ?
        //            SKColor(red: 0, green: 0.2, blue: absChargeValue*3.0/100.0+0.7, alpha: 1) :
        //            SKColor(red: absChargeValue*3.0/100.0+0.7, green: 0.2, blue: 0, alpha: 1)
        let color = SKColor(red: 0, green: 1, blue: 1, alpha: 1);
        let fx = force[0]
        let fy = force[1]
        let fabs = fx * fx + fy * fy
        let path = arrowPathFx(fx, fy: fy, vlen: myLog(fabs))
        self.path = path.cgPath
        self.strokeColor = color
        self.lineWidth = 3
        self.position = CGPoint(x: CGFloat(electron!.x), y: CGFloat(electron!.y))
    }
}
