//
//  LineParentNode.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import SpriteKit

public class LineParentNode: SKNode {
    private var lineNodesWithElectron: [Electron2D : [SKNode]]!
    private var placedArrowCountOfALineWithElectron: [Electron2D : [SKNode:Int]]!
    private let maxArrowCountOfALine: Int!
    
    override init() {
        lineNodesWithElectron = [Electron2D: [SKNode]]()
        placedArrowCountOfALineWithElectron = [Electron2D: [SKNode: Int]]()
        maxArrowCountOfALine = 5
        super.init()
        self.isHidden = true
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addLinesWithElectron(_ electron: Electron2D) {
        func makeLineNode() -> SKNode {
            let lineNode = SKShapeNode()
            lineNode.lineWidth = 3
            (0 ..< maxArrowCountOfALine).forEach { _ in
                lineNode.addChild(makeArrowNode())
            }
            return lineNode
        }
        
        func makeArrowNode() -> SKNode {
            let arrowNode = SKLabelNode()
            arrowNode.text = ">"
            arrowNode.fontName = "Menlo-Bold"
            arrowNode.fontSize = 40
            arrowNode.verticalAlignmentMode = .center
            return arrowNode
        }
        
        var placedArrowCountOfALine = [SKNode: Int]()
        var lineNodes = [SKNode]()
        electron.forEach() { line in
            let lineNode = makeLineNode()
            addChild(lineNode)
            placedArrowCountOfALine[lineNode] = 0
            lineNodes.append(lineNode)
        }
        placedArrowCountOfALineWithElectron[electron] = placedArrowCountOfALine
        lineNodesWithElectron[electron] = lineNodes
    }
    
    open func plotELines(_ arrowStep:Int) {
        func removeAllArrowNodesFromLineNode(_ lineNode: SKNode, electron: Electron2D) {
            lineNode.children.forEach { $0.isHidden = true }
            placedArrowCountOfALineWithElectron[electron]?[lineNode] = 0
        }
        
        func addArrowNodeToLineNode(_ position: ELDouble2D, line: Line2D, lineNode: SKNode, electron: Electron2D) {
            if let placedArrowCountOfALine = placedArrowCountOfALineWithElectron[electron] {
                if placedArrowCountOfALine[lineNode]! < maxArrowCountOfALine {
                    if let arrowAngle = line.arrowAngle(position){
                        let arrowNode = lineNode.children[placedArrowCountOfALine[lineNode]!]
                        placedArrowCountOfALineWithElectron[electron]?[lineNode] = placedArrowCountOfALine[lineNode]! + 1
                        arrowNode.isHidden = false
                        arrowNode.position = CGPoint(x: position.x, y: position.y)
                        arrowNode.zRotation = CGFloat(arrowAngle + (electron.charge > 0 ? 0.0 : Double.pi))
                    }
                }
            }
        }
        
        lineNodesWithElectron.forEach { (electron, lineNodes) in
            for line in electron {
                let path = BezierPath()
                let lineNode = lineNodes[line.index] as! SKShapeNode
                removeAllArrowNodesFromLineNode(lineNode, electron: electron)
                if line.firstPoint.x.isNaN || line.firstPoint.y.isNaN {
                    break
                } else {
                    path.move(to: CGPoint(x: line.firstPoint.x, y: line.firstPoint.y))
                }
                line.forEach { (point) in
                    path.line(to: CGPoint(x: point.x, y: point.y))
                    if point.index % arrowStep == arrowStep - 1 {
                        addArrowNodeToLineNode(point, line: line, lineNode: lineNode, electron: electron)
                    }
                }
                lineNode.path = path.cgPath
            }
        }
        isHidden = false
    }
    
    open func deleteLinesWithElectron(_ electron: Electron2D) {
        if let lineNodes = lineNodesWithElectron[electron] {
            lineNodes.forEach { lineNode in
                lineNode.removeFromParent()
            }
        }
        lineNodesWithElectron[electron] = nil
        placedArrowCountOfALineWithElectron[electron] = nil
    }
    
    open func replaceLinesWithElectron(_ electron: Electron2D) {
        deleteLinesWithElectron(electron)
        addLinesWithElectron(electron)
    }
    
    //    override open func removeFromParent() {
    //        _lineNodesWithElectron.forEach { (electron, _) in
    //            deleteLinesWithElectron(electron)
    //        }
    //    }
    //
    
}
