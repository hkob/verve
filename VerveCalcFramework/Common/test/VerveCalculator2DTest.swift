//
//  VerveCalculator2DTest.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import XCTest
import VerveCalc

class VerveCalculator2DTest: XCTestCase {
    private var vc: VerveCalculator2D?
    private let mx = 1024.0
    private let my = 698.0
    private let cx = 512.0
    private let cy = 349.0

    override func setUp() {
        super.setUp()
        vc = VerveCalculator2D(nCalc: 1000, dr: 0.1, step: 10)

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testVerveCalculator2Dが初期化できること() {
        func testInitVerveCalculator2D(_ nCalc: Int, dr: Double, step: Int){
            vc = VerveCalculator2D(nCalc: nCalc, dr: dr, step: step)
            XCTAssertEqual(vc!.nCalc, nCalc)
            XCTAssertEqual(vc!.dr, dr)
            XCTAssertEqual(vc!.step, step)
            XCTAssertEqual(vc!.nElectron, 0)
        }
        testInitVerveCalculator2D(1000, dr: 0.1, step: 10)
        testInitVerveCalculator2D(2000, dr: 0.3, step: 50)
        testInitVerveCalculator2D(10000, dr: 1.0E-2, step: 100)
    }
    
    func testCreateTemplateで電界テンプレートが作成できること() {
        vc!.createTemplate()
        XCTAssertEqual(vc!.exfields.count, Int(mx * my))
        XCTAssertEqual(vc!.eyfields.count, Int(mx * my))
    }

    func testAddElectronで電荷が追加できること() {
        let ec = VerveCalculator2D(nCalc: 1000, dr: 0.1, step: 10)
        let testE: [Electron2D] = [
            Electron2D(charge: 0.0, position: Point2D(x: 0.0, y: 0.0), division: 2, nPlot: 100),
            Electron2D(charge: 1.0, position: Point2D(x: 1.0, y: 1.0), division: 2, nPlot: 100),
            Electron2D(charge: 2.0, position: Point2D(x: -1.0, y: -1.0), division: 2, nPlot: 100)
        ]
        testE.enumerated().forEach { (i, e) in
            ec.addElectron(e)
            XCTAssertEqual(ec.nElectron, i + 1)
        }
        zip(ec, testE).forEach { (e, c) in
            XCTAssertEqual(e, c)
        }
    }

    func testRemoveElectronで電荷が削除できること() {
        let ec = VerveCalculator2D(nCalc: 1000, dr: 0.1, step: 10)
        let testE: [Electron2D] = [
            Electron2D(charge: 0.0, position: Point2D(x: 0.0, y: 0.0), division: 2, nPlot: 100),
            Electron2D(charge: 1.0, position: Point2D(x: 1.0, y: 1.0), division: 2, nPlot: 100),
            Electron2D(charge: 2.0, position: Point2D(x: -1.0, y: -1.0), division: 2, nPlot: 100),
            Electron2D(charge: 3.0, position: Point2D(x: -1.0, y: 1.0), division: 2, nPlot: 100),
            Electron2D(charge: 4.0, position: Point2D(x: 1.0, y: -1.0), division: 2, nPlot: 100)
        ]
        testE.forEach { (e) in
            ec.addElectron(e)
        }
        testE.reversed().enumerated().forEach { (i, re) in
            _ = ec.removeElectron(re)
            ec.forEach { (e) in
                XCTAssertNotEqual(e, re)
            }
            XCTAssertEqual(ec.nElectron, testE.count - i - 1)
        }
    }

    func testCalcExyでXYの位置の電界が計算できること() {
        let e0 : Double = 8.85418782e-12
        let ec = VerveCalculator2D(nCalc: 1000, dr: 1, step: 10)
        ec.createTemplate()
        [
            Electron2D(charge: 4.0 * Double.pi * e0, position: Point2D(x: cx, y: cy), division: 2, nPlot: 100),
            Electron2D(charge: 4.0 * Double.pi * e0, position: Point2D(x: cx + 1.0, y: cy + 1.0), division: 2, nPlot: 100)
        ].forEach { (e) in
                ec.addElectron(e)
        }
        let positions = [
            Point2D(x: cx + 1.0, y: cy),
            Point2D(x: cx, y: cy + 1.0)
        ]
        let correct = [
            Point2D(x: 1.0, y: -1.0),
            Point2D(x: -1.0, y: 1.0)
        ]
        zip(positions, correct).forEach { (p, c) in
            let ef = ec.calcExy(p)
            XCTAssertEqual(ef.x, c.x, accuracy: 1e-9)
            XCTAssertEqual(ef.y, c.y, accuracy: 1e-9)
            XCTAssertEqual(ef.r, c.r, accuracy: 1e-9)
        }
    }

    func testCalcNewPositionで新しいポジションが取得できること() {
        struct CnpData {
            var electron: Electron2D
            var ef: Point2D
            var step: Int
            var dr: Double
        }
        [
            CnpData(electron: Electron2D(charge: 1.0, position: Point2D(), division: 2, nPlot: 100),
                    ef: Point2D(x: 10.0, y: 10.0), step: 10, dr: 0.1),
            CnpData(electron: Electron2D(charge: -1.0, position: Point2D(), division: 2, nPlot: 100),
                    ef: Point2D(x: -10.0, y: -10.0), step: 100, dr: 0.01),
            CnpData(electron: Electron2D(charge: -0.1, position: Point2D(), division: 2, nPlot: 100),
                    ef: Point2D(x: 5.0, y: -5.0), step: 50, dr: 0.02)
            ].forEach { (d) in
                let e = d.electron
                let ef = d.ef
                let c = e.chargeSign / ef.r / Double(d.step)
                let ap = Point2D(x: c * ef.x + 1.0, y: c * ef.y + 1.0)
                var p = Point2D(x: 1.0, y: 1.0)
                let ec = VerveCalculator2D(nCalc: 1000, dr: d.dr, step: d.step)
                ec.calcNewPosition(d.electron, ef: ef, position: &p)
                XCTAssertEqual(p.x, ap.x, accuracy: 1e-9)
                XCTAssertEqual(p.y, ap.y, accuracy: 1e-9)
        }
    }

    func testWillContinueCalcで計算継続を判断できること() {
        let ec = VerveCalculator2D(nCalc:1000, dr:0.1, step:10)
        ec.createTemplate()
        let line = Line2D(nPlot: 3, theta: Double.pi/2, position: Point2D(x: cx, y: cy))
        ec.addElectron(Electron2D(charge: 1.0, position: Point2D(x: cx, y: cy), division: 2, nPlot: 100))
        ec.addElectron(Electron2D(charge: -1.0, position: Point2D(x: cx + 1.0, y: cy + 1.0), division: 2, nPlot: 100))
        ec.addElectron(Electron2D(charge: -0.1, position: Point2D(x: cx - 1.0, y: cy - 1.0), division: 2, nPlot: 100))
        struct WccData {
            var eabs: Double
            var position: Point2D
            var willContinue: Bool
        }
        [
            WccData(eabs: 10.0, position: Point2D(x: cx + 3.0, y: cy + 3.0), willContinue: true),
            WccData(eabs: 1e-10, position: Point2D(x: cx - 5.0, y: cy - 5.0), willContinue: false),
            WccData(eabs: 1.0, position: Point2D(x: cx + 0.1, y: cy), willContinue: false),
            WccData(eabs: 0.5, position: Point2D(x: cx - 1.1, y: cy - 1.0), willContinue: false),

            ].forEach { (d) in
                XCTAssertEqual(ec.willContinueCalc(d.eabs, line: line, pass: 1, position: d.position), d.willContinue)
        }
    }

    func testCalcAnELineで1ライン分の計算ができること() {
        let testELDouble : [ELDouble2D] = [
            ELDouble2D(x: cx, y: cy + 1.0, index: 0),
            ELDouble2D(x: cx, y: cy + 2.0, index: 1),
            ELDouble2D(x: cx, y: cy + 3.0, index: 2)
        ]
        let ec = VerveCalculator2D(nCalc:2, dr:1, step:1)
        ec.createTemplate()
        let e = Electron2D(charge: 4.0*Double.pi, position: Point2D(x: cx, y: cy), division: 2, nPlot: 3)
        ec.addElectron(e)
        let line = Line2D(nPlot: 3, theta: Double.pi/2, position: Point2D())
        ec.calcAnELine(e, line: line, pass: 1)

        XCTAssertEqual(line.usablePoint, 3)
        zip(line, testELDouble).forEach { (lp, ap) in
            XCTAssertEqual(lp.x, ap.x, accuracy: 1e-9)
            XCTAssertEqual(lp.y, ap.y, accuracy: 1e-9)
        }
    }

    func testCalcELinesで複数本の計算ができること() {
        let cso = cos(0.25)
        let sso = sin(0.25)
        let testELDouble : [[ELDouble2D]] = [
            [
                ELDouble2D(x: cx + cso, y: cy + sso, index: 0),
                ELDouble2D(x: cx + cso * 2, y: cy + sso * 2, index: 1),
                ELDouble2D(x: cx + cso * 3, y: cy + sso * 3, index: 2)
            ],
            [
                ELDouble2D(x: cx - cso, y: cy - sso, index: 0),
                ELDouble2D(x: cx - cso * 2, y: cy - sso * 2, index: 1),
                ELDouble2D(x: cx - cso * 3, y: cy - sso * 3, index: 2)
            ]
        ]

        let ec = VerveCalculator2D(nCalc:2, dr:1, step:1)
        ec.createTemplate()
        let e = Electron2D(charge: 4.0*Double.pi, position: Point2D(x: cx, y: cy), division: 2, nPlot: 3)
        ec.addElectron(e)
        ec.calcELines()

        zip(e, testELDouble).forEach { (line, positions) in
            zip(line, positions).forEach { (elp, position) in
                XCTAssertEqual(elp.x, position.x, accuracy: 1e-9)
                XCTAssertEqual(elp.y, position.y, accuracy: 1e-9)
            }
        }
    }

    func testAddSLCurrentでSLCurrent2Dが追加できること() {
        let ec = VerveCalculator2D(nCalc: 1000, dr: 0.1, step: 10)
        [
            SLCurrent2D(current: 1.0, position: Point2D(x: 1.0, y: 1.0)),
            SLCurrent2D(current: 2.0, position: Point2D(x: 2.0, y: 2.0)),
            SLCurrent2D(current: 3.0, position: Point2D(x: 3.0, y: 3.0)),
            ].forEach { (c) in
                ec.addSLCurrent(c)
        }
        XCTAssertEqual(ec.nSLCurrent, 3)
    }

    func testRremoveSLCurrentでSLCurrentが削除できること() {
        let ec = VerveCalculator2D(nCalc: 1000, dr: 0.1, step: 10)
        let testCurrent:[SLCurrent2D] = [
            SLCurrent2D(current: 1.0, position: Point2D(x: 1.0, y: 1.0)),
            SLCurrent2D(current: 2.0, position: Point2D(x: 2.0, y: 2.0)),
            SLCurrent2D(current: 3.0, position: Point2D(x: 3.0, y: 3.0))
        ]
        testCurrent.forEach { (c) in
            ec.addSLCurrent(c)
        }
        for c in testCurrent {
            let idx = ec.removeSLCurrent(c)
            XCTAssertEqual(idx!, 0)
        }
    }

    func testCalcHxyOfVerveCalculator2D() {
        let ec = VerveCalculator2D(nCalc: 1000, dr: 0.1, step: 10)
        let current = SLCurrent2D(current: 2.0*Double.pi, position: Point2D())
        let p = Point2D(x: 1.0, y: 1.0)

        ec.addSLCurrent(current)
        let h = ec.calcHxy(p)

        XCTAssertEqual(h.x, -0.5, accuracy: 1e-9)
        XCTAssertEqual(h.y, 0.5, accuracy: 1e-9)
        XCTAssertEqual(h.r, sqrt(2.0)/2.0, accuracy: 1e-9)
    }

//    func testPotentialOfVerveCalculator2D() {
//        let e0 : Double = 8.85418782e-12
//        let ec = VerveCalculator2D(nCalc: 1000, dr: 0.1, step: 10)
//        let e = Electron2D(charge: 4.0*Double.pi*e0, position: Point2D(x: 0.0, y: 0.0), division: 1, nPlot: 1)
//        let p1 = Point2D(x: 1.0, y: 0.0)
//        let p2 = Point2D(x: 0.0, y: 2.0)
//        ec.addElectron(e)
//        let v1 = ec.potential(p1)
//        let v2 = ec.potential(p2)
//        XCTAssertEqualWithAccuracy(v1, 1.0, accuracy: 1e-9)
//        XCTAssertEqualWithAccuracy(v2, 0.5, accuracy: 1e-9)
//    }
//
//    func testPotentialBetween2PointsOfVerveCalculator2D() {
//        let e0 : Double = 8.85418782e-12
//        let ec = VerveCalculator2D(nCalc: 1000, dr: 0.1, step: 10)
//        ec.addElectron(Electron2D(charge: 4.0*Double.pi*e0, position: Point2D(x: 0.0, y: 0.0), division: 1, nPlot: 1))
//        let p1 = Point2D(x: -1.0, y: 0.0)
//        let p2 = Point2D(x: -1.0, y: 0.0)
//        let v = ec.potentialBetween2Points(p1, and: p2)
//        XCTAssertEqualWithAccuracy(v, 0.0, accuracy: 1e-9)
//    }
//
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
