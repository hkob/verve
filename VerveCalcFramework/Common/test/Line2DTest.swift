//
//  Line2DTest.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/02.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import XCTest
import VerveCalc

class Line2DTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testLineが初期化できること() {
        let args = [10, 100, 0, -10]
        let correct = [10, 100, 1, 1]
        zip(args, correct).forEach { (arg, correct) in
            let l = Line2D(nPlot: arg, theta: 0.0, position: Point2D())
            XCTAssertEqual(l.nPlot, correct)
            XCTAssertEqual(l.usablePoint, 1)
        }
        
        struct LposData {
            let argX: Double
            let argY: Double
            let argTheta: Double
            let correctX: Double
            let correctY: Double
        }
        [ LposData(argX: 0.0, argY: 0.0, argTheta: 0.0, correctX: 1.0, correctY: 0.0),
          LposData(argX: 0.0, argY: 0.0, argTheta: Double.pi/2, correctX: 0.0, correctY: 1.0),
          LposData(argX: 0.0, argY: 0.0, argTheta: Double.pi, correctX: -1.0, correctY: 0.0),
          LposData(argX: 0.0, argY: 0.0, argTheta: Double.pi*3.0/2.0, correctX: 0.0, correctY: -1.0),
          LposData(argX: 1.0, argY: 1.0, argTheta: 0.0, correctX: 2.0, correctY: 1.0),
          LposData(argX: 1.0, argY: 1.0, argTheta: Double.pi/2, correctX: 1.0, correctY: 2.0),
          LposData(argX: 1.0, argY: 1.0, argTheta: Double.pi, correctX: 0.0, correctY: 1.0),
          LposData(argX: 1.0, argY: 1.0, argTheta: Double.pi*3.0/2.0, correctX: 1.0, correctY: 0.0) ].forEach { (d) in
            let l = Line2D(nPlot: 1, theta: d.argTheta, position: Point2D(x: d.argX, y: d.argY))
            l.forEach { (p) in
                XCTAssertEqual(p.x, d.correctX, accuracy: 1e-9)
                XCTAssertEqual(p.y, d.correctY, accuracy: 1e-9)
            }
        }
    }
    
    func testAddPointで点が追加できること() {
        let testApData : [ELDouble2D] = [
            ELDouble2D(x: 1.0, y: 0.0, index: 0),
            ELDouble2D(x: 1.0, y: 2.0, index: 1),
            ELDouble2D(x: 2.0, y: 4.0, index: 2),
            ELDouble2D(x: 4.0, y: 8.0, index: 3),
            ELDouble2D(x: 8.0, y: 16.0, index: 4),
            ELDouble2D(x: 16.0, y: 32.0, index: 5)
        ]
        let line = Line2D(nPlot: testApData.count, theta: 0.0, position: Point2D())
        testApData.enumerated().forEach { (index, d) in
            if index != 0 {
                line.addPoint(d.point)
            }
            XCTAssertEqual(line.usablePoint, index + 1)
        }
        line.addPoint(Point2D(x: 30.0, y: 60.0))
        XCTAssertEqual(line.usablePoint, line.nPlot)
        zip(line, testApData).forEach { (p, c) in
            XCTAssertEqual(p.description, c.description)
        }
    }

    func testClearでポイントセットがリセットされること() {
        let line1 = Line2D(nPlot: 3, theta: 0.0, position: Point2D())
        XCTAssertEqual(line1.usablePoint, 1)
        line1.addPoint(Point2D(x: 1.0, y: 2.0))
        XCTAssertEqual(line1.usablePoint, 2)
        line1.clear(Point2D(x: 1.0, y: 2.0))
        XCTAssertEqual(line1.usablePoint, 1)
        line1.addPoint(Point2D(x: -1.0, y: -2.0))
        line1.clear(Point2D(x: 1.0, y: 2.0))
        XCTAssertEqual(line1.usablePoint, 1)
    }

    func testArrowAngleでポイントごとの角度が取得できること() {
        let line = Line2D(nPlot: 5, theta: 0.0, position: Point2D())
        line.addPoint(Point2D(x: 2.0, y: 1.0))
        line.addPoint(Point2D(x: 1.0, y: 2.0))
        line.addPoint(Point2D(x: 0.0, y: 1.0))
        line.addPoint(Point2D(x: sqrt(3.0), y: 2.0))
        let angle = [45, 135, -135, 30].map { (d) in d / 180.0 * Double.pi }
        zip(line, angle).forEach { (arg) in
            let (p, a) = arg
            XCTAssertEqual(line.arrowAngle(p)!, a, accuracy: 1e-9)
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
