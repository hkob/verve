//
//  Point2DTest.swift
//  VerveCalcFramework
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/01.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import XCTest
import VerveCalc

class Point2DTest: XCTestCase {
    fileprivate var p: Point2D?
    
    override func setUp() {
        super.setUp()
        self.p = Point2D(x: 999.0, y: 666.0)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testコンストラクタが様々なパターンの引数に対応できること() {
        XCTAssertEqual(Point2D().description, "0.0, 0.0")
        XCTAssertEqual(Point2D(x: 1.0).description, "1.0, 0.0")
        XCTAssertEqual(Point2D(y: 2.0).description, "0.0, 2.0")
        XCTAssertEqual(Point2D(x: 1.0, y: 2.0).description, "1.0, 2.0")
    }
    
    func testSetValueXで座標が変更できること() {
        p!.setValueX(); XCTAssertEqual(p!.description, "0.0, 0.0")
        p!.setValueX(1.0); XCTAssertEqual(p!.description, "1.0, 0.0")
        p!.setValueX(y: 2.0); XCTAssertEqual(p!.description, "0.0, 2.0")
        p!.setValueX(1.0, y: 2.0); XCTAssertEqual(p!.description, "1.0, 2.0")
    }
    
    func testXYZのgetterプロパティに対応すること() {
        struct XyrData {
            let x: Double
            let y: Double
            let r: Double
        }
        [
            XyrData(x: 1.0, y: 2.0, r: sqrt(5.0)),
            XyrData(x: 3.0, y: 4.0, r: 5.0),
            XyrData(x: -6.0, y: 8.0, r: 10.0)
            ].forEach { (d) in
                p!.setValueX(d.x, y: d.y)
                XCTAssertEqual(p!.x, d.x)
                XCTAssertEqual(p!.y, d.y)
                XCTAssertEqual(p!.r, d.r)
                XCTAssertEqual(p!.angle, atan2(d.y, d.x))
        }
    }
    
    func testAddValueXで座標が加算できること() {
        p!.addValueX(); XCTAssertEqual(p!.description, "999.0, 666.0")
        p!.addValueX(1.0); XCTAssertEqual(p!.description, "1000.0, 666.0")
        p!.addValueX(y: 2.0); XCTAssertEqual(p!.description, "1000.0, 668.0")
        p!.addValueX(1.0, y: 2.0); XCTAssertEqual(p!.description, "1001.0, 670.0")
    }
    
    func testAddselfで座標が加算できること() {
        p!.addself(Point2D()); XCTAssertEqual(p!.description, "999.0, 666.0")
        p!.addself(Point2D(x: 1.0)); XCTAssertEqual(p!.description, "1000.0, 666.0")
        p!.addself(Point2D(y: 2.0)); XCTAssertEqual(p!.description, "1000.0, 668.0")
        p!.addself(Point2D(x: 1.0, y: 2.0)); XCTAssertEqual(p!.description, "1001.0, 670.0")
    }
    
    func testで＋＝で座標が加算できること() {
        p! += Point2D(); XCTAssertEqual(p!.description, "999.0, 666.0")
        p! += Point2D(x: 1.0); XCTAssertEqual(p!.description, "1000.0, 666.0")
        p! += Point2D(y: 2.0); XCTAssertEqual(p!.description, "1000.0, 668.0")
        p! += Point2D(x: 1.0, y: 2.0); XCTAssertEqual(p!.description, "1001.0, 670.0")
    }
    
    func testAddで座標が加算できること() {
        XCTAssertEqual(p!.add(Point2D()).description, "999.0, 666.0")
        XCTAssertEqual(p!.add(Point2D(x: 1.0)).description, "1000.0, 666.0")
        XCTAssertEqual(p!.add(Point2D(y: 2.0)).description, "999.0, 668.0")
        XCTAssertEqual(p!.add(Point2D(x: 1.0, y: 2.0)).description, "1000.0, 668.0")
    }
    
    func test＋で座標が加算できること() {
        XCTAssertEqual((p! + Point2D()).description, "999.0, 666.0")
        XCTAssertEqual((p! + Point2D(x: 1.0)).description, "1000.0, 666.0")
        XCTAssertEqual((p! + Point2D(y: 2.0)).description, "999.0, 668.0")
        XCTAssertEqual((p! + Point2D(x: 1.0, y: 2.0)).description, "1000.0, 668.0")
    }
    
    func testDiffで差が出力できること() {
        let a = Point2D(x: 1.0, y: 2.0)
        let b = Point2D(x: 3.0, y: 5.0)
        let c = a.diff(b)
        XCTAssertEqual(c.description, "-2.0, -3.0")
        let d = c.diff(a)
        XCTAssertEqual(d.description, "-3.0, -5.0")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
