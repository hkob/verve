//
//  BezierPath.swift
//  VerveCalc
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//  Original designed and coded by Kousuke FUKUMORI on 2015.
//  Optimazed and arranged by Hiroyuki KOBAYASHI on 2016.
//

import UIKit

class BezierPath: UIBezierPath {
    public func line(to: CGPoint) {
        addLine(to: to)
    }
}
