//
//  BezierPath.swift
//  VerveCalc
//
//  Created by Hiroyuki KOBAYASHI on 2018/09/06.
//

import Cocoa

class BezierPath: NSBezierPath {
    var cgPath: CGPath {
        get {
            return self.transformToCGPath()
        }
    }

    /// Transforms the NSBezierPath into a CGPathRef
    ///
    /// :returns: The transformed NSBezierPath
    fileprivate func transformToCGPath() -> CGPath {

        // Create path
        let path = CGMutablePath()
        let points = UnsafeMutablePointer<NSPoint>.allocate(capacity: 3)
        let numElements = self.elementCount
        if numElements > 0 {
            //            var didClosePath = true
            (0 ..< numElements).forEach { index in
                let pathType = self.element(at: index, associatedPoints: points)
                switch pathType {
                case .moveTo:
                    path.move(to: points[0])
                case .lineTo:
                    path.addLine(to: points[0])
                    //                    CGPathAddLineToPoint(path, nil, points[0].x, points[0].y)
                //                    didClosePath = false
                case .curveTo:
                    path.addCurve(to: points[0], control1: points[1], control2: points[2])
                    //                    CGPathAddCurveToPoint(path, nil, points[0].x, points[0].y, points[1].x, points[1].y, points[2].x, points[2].y)
                //                    didClosePath = false
                case .closePath:
                    path.closeSubpath()
                    //                    didClosePath = true
                }
            }
            //            if !didClosePath { CGPathCloseSubpath(path) }
        }
        points.deallocate()
        return path
    }
}
